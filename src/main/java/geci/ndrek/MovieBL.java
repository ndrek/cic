package geci.ndrek;

import com.google.gson.Gson;

import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class MovieBL {
    private Map<String, Movie> movieMap;
    private Gson gson;

    public MovieBL() {
        movieMap = importMovies();
        gson = new Gson();
    }

    /**
     *
     * @return
     */
    public Map<String, Movie> importMovies() {
        Map<String, Movie> movieMap = new HashMap<String, Movie>();

        URL url = null;
        try {
            // Definition of the URL with the JSON-Strings
            url = new URL("https://data.sfgov.org/resource/wwmu-gmzc.json");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        //if nodata is saved in URL
        if (url == null) {
            return new HashMap<String, Movie>();
        }

        // reader init and try catch blocks for errors
        InputStreamReader reader = null;
        try {
            reader = new InputStreamReader(url.openStream());
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (reader == null) {
            return new HashMap<String, Movie>();
        }

        /**
         * new movies array
         */
        Movie[] movies = new Gson().fromJson(reader, Movie[].class);

        /**
         * instered entryset to hashmap, title and movie obj
         */
        for (Movie movie : movies) {
            movieMap.put(movie.getTitle(), movie);
        }

        return movieMap;
    }

    /**
     *
     * @param movieName
     * @return
     */
    public String findMovieByName(String movieName) {
        return gson.toJson(movieMap.get(movieName));
    }

    /**
     * change location to a given movie name, if found in hashmap
     * @param MovieName
     * @param value
     */
    final void modify (final String MovieName, final String value){
       if(!movieMap.containsKey(MovieName)){
           return;
       }
        /**
         * get Movie obj to the given key, changed location
         */
        Movie movie = movieMap.get(MovieName);
        movie.setLocations(value);
        movieMap.put(MovieName,movie);
    }
}
