package geci.ndrek;

import java.util.Scanner;

import static java.io.FileDescriptor.out;

/**
 * Hello world!
 *
 */
public class App 
{

    /**
     * read lines until exit comes as user input to find location and title of the film
     * @param no return
     */
    public static void main( String[] args ) {
        MovieBL movieBL = new MovieBL();
        String input = "";
        String location = "";
        String name = "";
        Scanner scanner = new Scanner(System.in);

        while (!input.equals("exit")) {
            System.out.println("Write movie name to search for movie");
            System.out.println("Write ´modify´ modify values");
            System.out.println("Write exit to end application");


            /**
             * check for user input
             */
            if (!scanner.hasNextLine()) continue;

            /**
             * go to next line
             */
            input = scanner.nextLine(); // save UI into input

            /**
             * loaction to modify by movie name
             */
            if(input.equals("modify")){
                System.out.println("Name of movie");
                name = scanner.nextLine();
                System.out.println("Enter value to modify");
                location = scanner.nextLine(); // modify value
                movieBL.modify(name, location);
            }

            if (input.equals("exit")) break; // system exit

            System.out.printf(movieBL.findMovieByName(input)); // filter movie by name
        }
    }
}
