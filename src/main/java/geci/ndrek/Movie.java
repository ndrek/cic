package geci.ndrek;

public class Movie {

    private String title;
    private String locations;

    public Movie() {

    }

    /**
     * new constructor
     * @param title
     * @param locations
     */
    public Movie(String title, String locations) {
        this.title = title;
        this.locations = locations;
    }

    /**
     * getter and setter implemented
     * @return
     */
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLocations() {
        return locations;
    }

    public void setLocations(String locations) {
        this.locations = locations;
    }
}